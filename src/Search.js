import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Book from './Book'

const Search = (props) => {
  const { books, shelf, changeShelf, onChangeShelf, updateSearch } = props
  return (
    <div className="search-books">
      <div className="search-books-bar">
        <Link className="close-search" to="/">Close</Link>
        <div className="search-books-input-wrapper">
          <input
            type="text"
            placeholder="Search by title or author"
            onChange={updateSearch}
          />
        </div>
      </div>
      <div className="search-books-results">
        <ol className="books-grid">
          {books
            .map((book) => (
              <li key={book.id}>
                <Book
                  book={book}
                  shelf={shelf}
                  changeShelf={changeShelf}
                  onChangeShelf={onChangeShelf}
                />
              </li>
            ))}
        </ol>
      </div>
    </div>
  )
}

export default Search
