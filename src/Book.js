import React, { Component } from 'react'
import ShelfChanger from './ShelfChanger';

class Book extends Component {
  render() {
    const { book, changeShelf, onChangeShelf } = this.props
    return (
      <div className="book">
        <div className="book-top">
          <img
            src={book.imageLinks ? book.imageLinks.thumbnail : ""}
            alt={book.title}
            className="book-cover"
            style={{ width: 128, height: 193 }}>
          </img>
          <ShelfChanger
            shelf={book.shelf}
            book={book}
            changeShelf={changeShelf}
            onChangeShelf={onChangeShelf}
            />
        </div>
        <div className="book-title">{book.title}</div>
        <div className="book-authors">{book.authors}</div>
      </div>
    )
  }
}

export default Book
