import React, { Component } from 'react'

class ShelfChanger extends Component {
  onChangeShelf = (event) => {
    this.props.changeShelf(this.props.book, event.target.value)
  }
  render() {
    return (
      <div className="book-shelf-changer">
        <select defaultValue={this.props.shelf} onChange={this.onChangeShelf}>
          <option value="move" disabled>Move to...</option>
          <option value="currentlyReading">Currently Reading</option>
          <option value="wantToRead">Want to Read</option>
          <option value="read">Read</option>
          <option value="none">None</option>
        </select>
      </div>
    )
  }
}

export default ShelfChanger
