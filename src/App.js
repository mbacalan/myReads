import React from 'react'
import { Route, Link } from 'react-router-dom'
import * as BooksAPI from './BooksAPI'
import Search from './Search'
import Shelf from './Shelf';
import './App.css'

class BooksApp extends React.Component {
  state = {
    listedBooks: [],
    searchedBooks: [],
  }

  componentDidMount() {
    BooksAPI.getAll().then((books) => {
      this.setState({ listedBooks: books })
    })
  }

  updateSearch = (query) => {
    let searchingBooks = []
    if (query) {
      BooksAPI.search(query).then(books => {
        if (books.error) {
          this.setState({ searchedBooks: [] })
        } else {
          searchingBooks = books.map(book => {
            book.shelf = this.syncSearchShelf(book);
            return book;
          })
          this.setState({ searchedBooks: searchingBooks })
        }
      })
    } else if (!query) {
      this.setState({ searchedBooks: [] })
    }
  }

  syncSearchShelf = (book) => {
    let shelvedBook = this.state.listedBooks.filter(listedBook =>
      book.id === listedBook.id
    )
    if (shelvedBook.length) {
      return shelvedBook[0].shelf
    } else if (!shelvedBook.length) {
      return 'none'
    }
  }

  changeShelf = (book, shelf) => {
    BooksAPI.update(book, shelf);
    BooksAPI.getAll().then((books) => {
      this.setState({ listedBooks: books })
    })
  }

  render() {
    return (
      <div className="app">
        <Route path="/search" render={() => (
          <Search
            books={this.state.searchedBooks}
            updateSearch={(event) => this.updateSearch(event.target.value)}
            changeShelf={this.changeShelf}
            onChangeShelf={(book, shelf) => this.changeShelf(book, shelf)}
          />
        )}
        />

        <Route exact path="/" render={() => (
          <div className="list-books">
            <div className="list-books-title">
              <h1>MyReads</h1>
            </div>
            <div className="list-books-content">
              <div>
                <Shelf
                  title="Currently Reading"
                  shelf="currentlyReading"
                  books={this.state.listedBooks}
                  changeShelf={this.changeShelf}
                  onChangeShelf={(book, shelf) => this.changeShelf(book, shelf)}
                />
                <Shelf
                  title="Want to Read"
                  shelf="wantToRead"
                  books={this.state.listedBooks}
                  changeShelf={this.changeShelf}
                  onChangeShelf={(book, shelf) => this.changeShelf(book, shelf)}
                />
                <Shelf
                  title="Read"
                  shelf="read"
                  books={this.state.listedBooks}
                  changeShelf={this.changeShelf}
                  onChangeShelf={(book, shelf) => this.changeShelf(book, shelf)}
                />

                <div className="open-search">
                  <Link to="/search">Add a book</Link>
                </div>

              </div>
            </div>
          </div>
        )} />
      </div>
    )
  }
}

export default BooksApp
