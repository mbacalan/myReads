# MyReads - Book Tracking App

A React application to track your books and keep them organized.

Using Google Books API, it lets you search for books and keep track of them by making use of Shelves;

 * Currently Reading
 * Want to Read
 * Read


## Installation

```bash
# Get dependencies:
yarn install

#Start a development server:
yarn start

#Build for production:
yarn build

#Commands are interchangable with npm
```

## Notes

This project is built upon the base code provided by [Udacity](https://www.udacity.com/) under the Front-End Web Developer Nanodegree.
